﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar_Range : MonoBehaviour
{
    public GameObject HealthBar;
    public int HealthCount;
    public Sprite[] HealthSteps;

    public string CopyPasterino_Red;
    public string CopyPasterino_DarkRed;

    public void Start()
    {
        HealthCount = 100;
    }

    public void Update()
    {
        //enable or disable
        if (HealthCount > 0)
        {
            HealthBar.gameObject.SetActive(true);
        }
        else
        {
            HealthBar.gameObject.SetActive(false);
        }


        //set the sprites
        if (HealthCount == 100)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[0];
        }

        else if (HealthCount == 99)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[1];
        }

        else if (HealthCount == 98)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[2];
        }

        else if (HealthCount == 97)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[3];
        }

        else if (HealthCount == 96)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[4];
        }

        else if (HealthCount == 95)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[5];
        }

        else if (HealthCount == 94)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[6];
        }

        else if (HealthCount == 93)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[7];
        }

        else if (HealthCount == 92)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[8];
        }

        else if (HealthCount == 91)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[9];
        }

        else if (HealthCount == 90)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[10];
        }

        else if (HealthCount == 89)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[11];
        }

        else if (HealthCount == 88)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[12];
        }

        else if (HealthCount == 87)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[13];
        }

        else if (HealthCount == 86)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[14];
        }

        else if (HealthCount == 85)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[15];
        }

        else if (HealthCount == 84)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[16];
        }

        else if (HealthCount == 83)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[17];
        }

        else if (HealthCount == 82)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[18];
        }

        else if (HealthCount == 81)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[19];
        }

        else if (HealthCount == 80)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[20];
        }

        else if (HealthCount == 79)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[21];
        }

        else if (HealthCount == 78)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[22];
        }

        else if (HealthCount == 77)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[23];
        }

        else if (HealthCount == 76)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[24];
        }

        else if (HealthCount == 75)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[25];
        }

        else if (HealthCount == 74)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[26];
        }

        else if (HealthCount == 73)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[27];
        }

        else if (HealthCount == 72)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[28];
        }

        else if (HealthCount == 71)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[29];
        }

        else if (HealthCount == 70)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[30];
        }

        else if (HealthCount == 69)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[31];
        }

        else if (HealthCount == 68)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[32];
        }

        else if (HealthCount == 67)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[33];
        }

        else if (HealthCount == 66)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[35];
        }

        else if (HealthCount == 65)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[36];
        }

        else if (HealthCount == 64)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[37];
        }

        else if (HealthCount == 63)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[38];
        }

        else if (HealthCount == 62)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[39];
        }

        else if (HealthCount == 61)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[40];
        }

        else if (HealthCount == 60)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[41];
        }

        else if (HealthCount == 59)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[42];
        }

        else if (HealthCount == 58)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[43];
        }

        else if (HealthCount == 57)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[44];
        }

        else if (HealthCount == 56)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[45];
        }

        else if (HealthCount == 55)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[46];
        }

        else if (HealthCount == 54)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[47];
        }

        else if (HealthCount == 53)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[48];
        }

        else if (HealthCount == 52)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[49];
        }

        else if (HealthCount == 51)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[50];
        }

        else if (HealthCount == 50)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[51];
        }

        else if (HealthCount == 49)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[52];
        }

        else if (HealthCount == 48)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[53];
        }

        else if (HealthCount == 47)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[54];
        }

        else if (HealthCount == 46)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[55];
        }

        else if (HealthCount == 45)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[56];
        }

        else if (HealthCount == 44)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[57];
        }

        else if (HealthCount == 43)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[58];
        }

        else if (HealthCount == 42)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[59];
        }

        else if (HealthCount == 41)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[60];
        }

        else if (HealthCount == 40)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[61];
        }

        else if (HealthCount == 39)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[62];
        }

        else if (HealthCount == 38)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[63];
        }

        else if (HealthCount == 37)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[64];
        }

        else if (HealthCount == 36)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[65];
        }

        else if (HealthCount == 35)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[66];
        }

        else if (HealthCount == 34)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[67];
        }

        else if (HealthCount == 33)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[68];
        }

        else if (HealthCount == 32)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[69];
        }

        else if (HealthCount == 31)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[70];
        }

        else if (HealthCount == 30)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[71];
        }

        else if (HealthCount == 29)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[72];
        }

        else if (HealthCount == 28)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[73];
        }

        else if (HealthCount == 27)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[74];
        }

        else if (HealthCount == 26)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[75];
        }

        else if (HealthCount == 25)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[76];
        }

        else if (HealthCount == 24)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[77];
        }

        else if (HealthCount == 23)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[78];
        }

        else if (HealthCount == 22)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[79];
        }

        else if (HealthCount == 21)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[80];
        }

        else if (HealthCount == 20)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[81];
        }

        else if (HealthCount == 19)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[82];
        }

        else if (HealthCount == 18)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[83];
        }

        else if (HealthCount == 17)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[84];
        }

        else if (HealthCount == 16)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[85];
        }

        else if (HealthCount == 15)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[86];
        }
        
        else if (HealthCount == 14)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[87];
        }

        else if (HealthCount == 13)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[88];
        }

        else if (HealthCount == 12)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[89];
        }

        else if (HealthCount == 11)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[90];
        }

        else if (HealthCount == 10)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[91];
        }

        else if (HealthCount == 9)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[92];
        }

        else if (HealthCount == 8)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[93];
        }

        else if (HealthCount == 7)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[94];
        }

        else if (HealthCount == 6)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[95];
        }

        else if (HealthCount == 5)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[96];
        }

        else if (HealthCount == 4)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[97];
        }

        else if (HealthCount == 3)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[98];
        }

        else if (HealthCount == 2)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[99];
        }

        else if (HealthCount == 1)
        {
            HealthBar.GetComponent<Image>().sprite = HealthSteps[100];
        }
    }
}
