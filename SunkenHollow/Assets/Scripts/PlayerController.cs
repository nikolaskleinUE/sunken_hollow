﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public float moveSpeed;

	private Animator anim;

	private bool isMoving;
    //private Vector2 lastMove;

    private Animator animController;
    public RuntimeAnimatorController[] States;

    public Canvas MenuCanvas;

    public bool MenuCanvasTriggered;

	// Use this for initialization
	void Start()
	{
        animController = GetComponent<Animator>();
		anim = GetComponent<Animator>();

        MenuCanvas.gameObject.SetActive(false);
        MenuCanvasTriggered = false;
    }

    // Update is called once per frame
    void Update()
	{
		isMoving = false;

		if (Input.GetAxisRaw("Horizontal") > 0f)
		{
			transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime, 0f, 0f));
			isMoving = true;
			//lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), 0f);
			anim.SetFloat("LastX", 1);
		}

		if (Input.GetAxisRaw("Horizontal") < 0f)
		{
			transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime, 0f, 0f));
			isMoving = true;
			//lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), 0f);
			anim.SetFloat("LastX", -1);
		}

		if (Input.GetAxisRaw("Vertical") > 0f)
		{
			transform.Translate(new Vector3(0f, Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime, 0f));
			isMoving = true;
			//lastMove = new Vector2(0f, Input.GetAxisRaw("Vertical"));
			anim.SetFloat("LastY", 1);
		}

		if (Input.GetAxisRaw("Vertical") < 0f)
		{
			transform.Translate(new Vector3(0f, Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime, 0f));
			isMoving = true;
			//lastMove = new Vector2(0f, Input.GetAxisRaw("Vertical"));
			anim.SetFloat("LastY", -1);
		}

		anim.SetFloat("Xspeed", Input.GetAxisRaw("Horizontal"));
		anim.SetFloat("Yspeed", Input.GetAxisRaw("Vertical"));
		anim.SetBool("isMoving", isMoving);
        //anim.SetFloat("LastX", lastMove.x);
        //anim.SetFloat("LastY", lastMove.y);


        //toggle menu canvas
        if (Input.GetKeyDown(KeyCode.Escape) && MenuCanvasTriggered == false)
        {
            MenuCanvasTriggered = true;
            MenuCanvas.gameObject.SetActive(true);
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && MenuCanvasTriggered == true)
        {
            MenuCanvasTriggered = false;
            MenuCanvas.gameObject.SetActive(false);
        }
	}
}
