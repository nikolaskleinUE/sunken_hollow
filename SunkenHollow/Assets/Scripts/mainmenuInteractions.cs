﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainmenuInteractions : MonoBehaviour {

    public void PlayClick()
    {
        SceneManager.LoadScene("testscene");
    }

    public void OptionsClick()
    {

    }

    public void CreditsClick()
    {

    }

    public void ExitClick()
    {
        Application.Quit();
    }
}
