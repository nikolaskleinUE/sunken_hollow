﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSword2 : MonoBehaviour
{
    //option to pick which sword it should be
    public bool isGreySword;
    public bool isBlueSword;
    public bool isRedSword;
    public bool isGreenSword;
    public bool isYellowSword;
    public bool isPinkSword;
    public bool isCyanSword;

    //player pick up ui
    public GameObject PickUpImage;

    //get the slot
    public GameObject Slot;

    //array storing the images for the visual representation of the sword
    public Sprite[] SwordImages;

    //checking the tick box which you can change in the inspector to set the according name and sprite
    public void Start()
    {
        //disable pick up ui on start
        PickUpImage.gameObject.SetActive(false);

        if (isGreySword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[0];
        }

        else if (isBlueSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[1];
        }

        else if (isRedSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[2];
        }

        else if (isGreenSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[3];
        }

        else if (isYellowSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[4];
        }

        else if (isPinkSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[5];
        }
        
        else if (isCyanSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[6];
        }
    }

    //checking for the name to do stuff
    public void OnTriggerStay2D(Collider2D target)
    {
        PickUpImage.gameObject.SetActive(true);

        if (PickUpImage.gameObject.active && Input.GetButtonDown("InteractButton"))
        {
            if (isGreySword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "GreySword";
                PickUpImage.gameObject.SetActive(false);
            }

            if (isBlueSword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "BlueSword";
                PickUpImage.gameObject.SetActive(false);
            }

            if (isRedSword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "RedSword";
                PickUpImage.gameObject.SetActive(false);
            }

            if (isGreenSword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "GreenSword";
                PickUpImage.gameObject.SetActive(false);
            }

            if (isYellowSword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "YellowSword";
                PickUpImage.gameObject.SetActive(false);
            }

            if (isPinkSword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "PinkSword";
                PickUpImage.gameObject.SetActive(false);
            }

            if (isCyanSword)
            {
                Destroy(this.gameObject);
                Slot.gameObject.GetComponent<WeaponInventory2>().CurrItem = "CyanSword";
                PickUpImage.gameObject.SetActive(false);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        PickUpImage.gameObject.SetActive(false);
    }
}
