﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsumableInventory : MonoBehaviour
{
    public Sprite[] ConsumableSlots;

    public GameObject ConsumableSlot;

    public Sprite Slot_Null;

    public bool pickedUpPotion;

    public Text ConsumableSlotText;

    void Start()
    {
        pickedUpPotion = false;

        ConsumableSlot.GetComponent<Image>().sprite = Slot_Null;
        ConsumableSlotText.text = "EMPTY";
    }

    void Update()
    {
        if (pickedUpPotion == true)
        {
            ConsumableSlot.GetComponent<Image>().sprite = ConsumableSlots[0];
            ConsumableSlotText.text = "HEALTH POTION";
        }
        else
        {
            ConsumableSlot.GetComponent<Image>().sprite = Slot_Null;
            ConsumableSlotText.text = "EMPTY";
        }
    }
}