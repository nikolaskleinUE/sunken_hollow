﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponInventory : MonoBehaviour
{
    public Sprite[] weaponSlots;

    public GameObject WeaponSlot;

    public Sprite Slot_Null;

    //Animation Stuff
    private Animator anim;
    public GameObject Player;

    private Animator animController;
    public RuntimeAnimatorController[] States;

    public bool[] pickedUpSword;

    public Text WeaponSlotText;

    void Start()
    {
        //Animation Stuff
        animController = Player.GetComponent<Animator>();
        anim = GetComponent<Animator>();

        pickedUpSword[0] = false;
        pickedUpSword[1] = false;
        pickedUpSword[2] = false;
        pickedUpSword[3] = false;
        pickedUpSword[4] = false;
        pickedUpSword[5] = false;
        pickedUpSword[6] = false;

        WeaponSlot.GetComponent<Image>().sprite = Slot_Null;
        WeaponSlotText.text = "EMPTY";
        animController.runtimeAnimatorController = States[0];
    }

    void Update()
    {
        if (pickedUpSword[0] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[0];
            WeaponSlotText.text = "GREY SWORD";
            animController.runtimeAnimatorController = States[1];
        }
        else if (pickedUpSword[1] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[1];
            WeaponSlotText.text = "BLUE SWORD";
            animController.runtimeAnimatorController = States[2];
        }
        else if (pickedUpSword[2] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[2];
            WeaponSlotText.text = "RED SWORD";
            animController.runtimeAnimatorController = States[3];
        }
        else if (pickedUpSword[3] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[3];
            WeaponSlotText.text = "GREEN SWORD";
            animController.runtimeAnimatorController = States[4];
        }
        else if (pickedUpSword[4] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[4];
            WeaponSlotText.text = "YELLOW SWORD";
            animController.runtimeAnimatorController = States[5];
        }
        else if (pickedUpSword[5] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[5];
            WeaponSlotText.text = "PINK SWORD";
            animController.runtimeAnimatorController = States[6];
        }
        else if (pickedUpSword[6] == true)
        {
            WeaponSlot.GetComponent<Image>().sprite = weaponSlots[6];
            WeaponSlotText.text = "CYAN SWORD";
            animController.runtimeAnimatorController = States[7];
        }
        else
        {
            WeaponSlot.GetComponent<Image>().sprite = Slot_Null;
            WeaponSlotText.text = "EMPTY";
            animController.runtimeAnimatorController = States[0];
        }
    }
}