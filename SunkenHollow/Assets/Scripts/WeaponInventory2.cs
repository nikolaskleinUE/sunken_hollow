﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponInventory2 : MonoBehaviour
{
    //string storing the current sword
    public string CurrItem;

    //array storing the images for the sword ui and ui text for the name of the sword
    public Sprite[] UIImages;
    public Text WeaponSlotText;

    //the gameobject of the slot
    public GameObject UISlot;

    //animation stuff
    private Animator anim;
    public GameObject Player;
    private Animator animController;
    public RuntimeAnimatorController[] States;

    public void Start()
    {
        //animation stuff
        animController = Player.GetComponent<Animator>();
        anim = GetComponent<Animator>();

        //set CurrItem to null when the game starts (= has no weapon)
        CurrItem = null;

        UISlot.GetComponent<Image>().sprite = UIImages[7]; //on start, set the ui image of the slot to the empty one
        WeaponSlotText.text = "EMPTY"; //on start, set the ui text of the item to EMPTY
        animController.runtimeAnimatorController = States[0]; //on start, set the player controller to the one without a sword
    }

    public void Update()
    {
        if (CurrItem == null)
        {
            //Debug.Log("No Weapon Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[7];
            WeaponSlotText.text = "EMPTY";
            animController.runtimeAnimatorController = States[0];
        }

        else if (CurrItem == "GreySword")
        {
            //Debug.Log("Grey Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[0];
            WeaponSlotText.text = "GREY SWORD";
            animController.runtimeAnimatorController = States[1];
        }

        else if (CurrItem == "BlueSword")
        {
            //Debug.Log("Blue Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[1];
            WeaponSlotText.text = "BLUE SWORD";
            animController.runtimeAnimatorController = States[2];
        }

        else if (CurrItem == "RedSword")
        {
            //Debug.Log("Red Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[2];
            WeaponSlotText.text = "RED SWORD";
            animController.runtimeAnimatorController = States[3];
        }

        else if (CurrItem == "GreenSword")
        {
            //Debug.Log("Green Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[3];
            WeaponSlotText.text = "GREEN SWORD";
            animController.runtimeAnimatorController = States[4];
        }

        else if (CurrItem == "YellowSword")
        {
            //Debug.Log("Yellow Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[4];
            WeaponSlotText.text = "YELLOW SWORD";
            animController.runtimeAnimatorController = States[5];
        }

        else if (CurrItem == "PinkSword")
        {
            //Debug.Log("Pink Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[5];
            WeaponSlotText.text = "PINK SWORD";
            animController.runtimeAnimatorController = States[6];
        }

        else if (CurrItem == "CyanSword")
        {
            //Debug.Log("Cyan Sword Equipped");
            UISlot.GetComponent<Image>().sprite = UIImages[6];
            WeaponSlotText.text = "CYAN SWORD";
            animController.runtimeAnimatorController = States[7];
        }
    }
}