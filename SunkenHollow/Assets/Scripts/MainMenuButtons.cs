﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour
{
    public int Selection;
    public int MaxSelection = 3;

    public Text PlayText;
    public Text OptionsText;
    public Text CreditsText;
    public Text QuitText;

	void Start ()
    {
        Selection = 0;
	}
	
	void Update ()
    {
        if (Input.GetButtonDown("Down") /*< -0.10*/) //DOWN
        {
            if (Selection > 3)
            {
                Selection = 0;
            }
            else
            {
                Selection = Selection + 1;
            }
        }

        if (Input.GetButtonDown("Up") /*> 0.10*/) //UP
        {
            if (Selection < 0)
            {
                Selection = 3;
            }
            else
            {
                Selection = Selection - 1;
            }
        }

        //color the texts
        if (Selection == 0)
        {
            PlayText.color = Color.white;
            OptionsText.color = Color.gray;
            CreditsText.color = Color.gray;
            QuitText.color = Color.gray;
        }

        if (Selection == 1)
        {
            PlayText.color = Color.gray;
            OptionsText.color = Color.white;
            CreditsText.color = Color.gray;
            QuitText.color = Color.gray;
        }

        if (Selection == 2)
        {
            PlayText.color = Color.gray;
            OptionsText.color = Color.gray;
            CreditsText.color = Color.white;
            QuitText.color = Color.gray;
        }

        if (Selection == 3)
        {
            PlayText.color = Color.gray;
            OptionsText.color = Color.gray;
            CreditsText.color = Color.gray;
            QuitText.color = Color.white;
        }
    }
}
