﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSword : MonoBehaviour
{
    public GameObject WeaponSlot;

    public bool isGreySword;
    public bool isBlueSword;
    public bool isRedSword;
    public bool isGreenSword;
    public bool isYellowSword;
    public bool isPinkSword;
    public bool isCyanSword;

    public bool[] SwordBools;

    public Sprite[] SwordImages;

    void Start()
    {
        SwordBools[0] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[0];
        SwordBools[1] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[1];
        SwordBools[2] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[2];
        SwordBools[3] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[3];
        SwordBools[4] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[4];
        SwordBools[5] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[5];
        SwordBools[6] = WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[6];
    }

    void Update()
    {
        if (isGreySword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[0];
        }
        else if (isBlueSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[1];
        }
        else if (isRedSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[2];
        }
        else if (isGreenSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[3];
        }
        else if (isYellowSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[4];
        }
        else if (isPinkSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[5];
        }
        else if (isCyanSword)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SwordImages[6];
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (isGreySword)
        {
            SwordBools[0] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[0] = true;
        }
        else if (isBlueSword)
        {
            SwordBools[1] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[1] = true;
        }
        else if (isRedSword)
        {
            SwordBools[2] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[2] = true;
        }
        else if(isGreenSword)
        {
            SwordBools[3] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[3] = true;
        }
        else if (isYellowSword)
        {
            SwordBools[4] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[4] = true;
        }
        else if (isPinkSword)
        {
            SwordBools[5] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[5] = true;
        }
        else if (isCyanSword)
        {
            SwordBools[6] = true;
            WeaponSlot.GetComponent<WeaponInventory>().pickedUpSword[6] = true;
        }
    }
}