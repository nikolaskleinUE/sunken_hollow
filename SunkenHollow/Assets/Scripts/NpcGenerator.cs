﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcGenerator : MonoBehaviour
{
    //option to pick which sword it should be
    public bool isMale;
    public bool isFemaleGrey;
    public bool isFemaleBlue;
    public bool isFemaleRed;
    public bool isFemaleGreen;
    public bool isFemaleYellow;
    public bool isFemalePink;
    public bool isFemaleCyan;
    public bool isOldmanGrey;
    public bool isOldmanBlue;
    public bool isOldmanRed;
    public bool isOldmanGreen;
    public bool isOldmanYellow;
    public bool isOldmanPink;
    public bool isOldmanCyan;
    public bool isBlacksmithGrey;
    public bool isBlacksmithRed;
    public bool isBlacksmithGreen;
    public bool isBlacksmithBlue;

    //player pick up ui
    public GameObject PickUpImage;

    //array storing the images for the visual representation of the sword
    public Sprite[] NpcImages;

    //checking the tick box which you can change in the inspector to set the according name and sprite
    public void Start()
    {
        //disable pick up ui on start
        PickUpImage.gameObject.SetActive(false);

        if (isMale)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[0];
        }

        else if (isFemaleGrey)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[1];
        }

        else if (isFemaleBlue)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[2];
        }

        else if (isFemaleRed)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[3];
        }

        else if (isFemaleGreen)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[4];
        }

        else if (isFemaleYellow)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[5];
        }

        else if (isFemalePink)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[6];
        }

        else if (isFemaleCyan)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[7];
        }

        else if (isOldmanGrey)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[8];
        }

        else if (isOldmanBlue)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[9];
        }

        else if (isOldmanRed)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[10];
        }

        else if (isOldmanGreen)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[11];
        }

        else if (isOldmanYellow)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[12];
        }

        else if (isOldmanPink)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[13];
        }

        else if (isOldmanCyan)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[14];
        }

        else if (isBlacksmithGrey)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[15];
        }

        else if (isBlacksmithRed)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[16];
        }

        else if (isBlacksmithGreen)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[17];
        }

        else if (isBlacksmithBlue)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = NpcImages[18];
        }
    }

    //checking for the name to do stuff
    public void OnTriggerStay2D(Collider2D target)
    {
        PickUpImage.gameObject.SetActive(true);

        if (PickUpImage.gameObject.active && Input.GetButtonDown("InteractButton"))
        {
            //
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        PickUpImage.gameObject.SetActive(false);
    }
}
