﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapSelectionButtons : MonoBehaviour
{
    public int Selection;
    public int MaxSelection = 3;

    public Text DestinationText;
    public Text LeftDestinationText;
    public Text RightDestinationText;
    public Image LocationMap;
    public Sprite[] LocationMapImage;

    public int UnlockState;

    void Start()
    {
        Selection = 0;
        UnlockState = 3;
    }

    void Update()
    {
        if (Input.GetButtonDown("Right") /*< -0.10*/) //DOWN
        {
            if (Selection > 3)
            {
                Selection = 3;
            }
            else
            {
                Selection = Selection + 1;
            }
        }

        if (Input.GetButtonDown("Left") /*> 0.10*/) //UP
        {
            if (Selection < 0)
            {
                Selection = 0;
            }
            else
            {
                Selection = Selection - 1;
            }
        }

        //color the texts
        if (Selection == 0)
        {
            if (UnlockState == 3)
            {
                DestinationText.text = "NIGHTVAULT";
                LeftDestinationText.text = null;
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[0];
            }
            else if (UnlockState == 2)
            {
                DestinationText.text = "- LOCKED -";
                LeftDestinationText.text = null;
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[4];
            }
            else if (UnlockState == 1)
            {
                DestinationText.text = "- LOCKED -";
                LeftDestinationText.text = null;
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[8];
            }
            else if (UnlockState == 0)
            {
                DestinationText.text = "- LOCKED -";
                LeftDestinationText.text = null;
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[12];
            }
}

        if (Selection == 1)
        {
            if (UnlockState == 3)
            {
                DestinationText.text = "SUNKEN HOLLOW";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[1];
            }
            else if (UnlockState == 2)
            {
                DestinationText.text = "SUNKEN HOLLOW";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[5];
            }
            else if (UnlockState == 1)
            {
                DestinationText.text = "SUNKEN HOLLOW";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[9];
            }
            else if (UnlockState == 0)
            {
                DestinationText.text = "SUNKEN HOLLOW";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[13];
            }
        }

        if (Selection == 2)
        {
            if (UnlockState == 3)
            {
                DestinationText.text = "SOULBLIGHT";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[2];
            }
            else if (UnlockState == 2)
            {
                DestinationText.text = "SOULBLIGHT";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[6];
            }
            else if (UnlockState == 1)
            {
                DestinationText.text = "- LOCKED -";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[10];
            }
            else if (UnlockState == 0)
            {
                DestinationText.text = "- LOCKED -";
                LeftDestinationText.text = "<";
                RightDestinationText.text = ">";
                LocationMap.sprite = LocationMapImage[14];
            }
        }

        if (Selection == 3)
        {
            if (UnlockState == 3)
            {
                DestinationText.text = "WICKED WOODS";
                LeftDestinationText.text = "<";
                RightDestinationText.text = null;
                LocationMap.sprite = LocationMapImage[3];
            }
            else if (UnlockState == 2)
            {
                DestinationText.text = "WICKED WOODS";
                LeftDestinationText.text = "<";
                RightDestinationText.text = null;
                LocationMap.sprite = LocationMapImage[7];
            }
            else if (UnlockState == 1)
            {
                DestinationText.text = "WICKED WOODS";
                LeftDestinationText.text = "<";
                RightDestinationText.text = null;
                LocationMap.sprite = LocationMapImage[11];
            }
            else if (UnlockState == 0)
            {
                DestinationText.text = "- LOCKED -";
                LeftDestinationText.text = "<";
                RightDestinationText.text = null;
                LocationMap.sprite = LocationMapImage[15];
            }
        }
    }
}
