﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthChange : MonoBehaviour
{
    private float yPosition;
    private float xPosition;

	void Update ()
    {
        xPosition = transform.position.x;
        yPosition = transform.position.y;

        transform.position = new Vector3(xPosition, yPosition, yPosition);
	}
}
