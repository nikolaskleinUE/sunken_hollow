﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashChange : MonoBehaviour
{
    public bool Slashed;

    public Sprite New;
    public Sprite Defective;

    public void Start()
    {

    }

    public void Update()
    {
        if (!Slashed)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = New;
        }

        else if (Slashed)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = Defective;
        }
    }
}
