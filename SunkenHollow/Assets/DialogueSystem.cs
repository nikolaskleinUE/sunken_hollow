﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    public string Name;
    public Text NameText;

    public string[] Sentences;
    public Text DialogueText;

    private int index;
    public float typingSpeed;

    public void Start()
    {
        NameText.text = Name;
    }

    IEnumerator Type()
    {
        foreach (char letter in Sentences[index].ToCharArray())
        {
            DialogueText.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    {
        if(index < Sentences.Length -1)
        {
            index++;
            DialogueText.text = "";
            StartCoroutine(Type());
        }

        else
        {
            DialogueText.text = "";
        }
    }
}
