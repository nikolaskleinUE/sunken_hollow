CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation/Setup
 * Licenses/Tools
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

'Sunken Hollow' is a 2d souls-like, set in the fictive town of Sunken Hollow - a world created by Nikolas Klein for a first semester - project.

 * For a full description and screenshots of the project:
   //insert link here

 * To track changes or view our Project Branches:
   https://bitbucket.org/nikolaskleinUE/sunken_hollow_nikolasklein/src


REQUIREMENTS
------------

This project requires the following software & hardware to be opened/run:

 * Unity 2017.2.0f3 or higher (https://unity3d.com/de/get-unity/download)
 * A machine running windows 7, Mac OsX El Capitan, (Linux) or higher (older operating systems MIGHT work, they were not tested though)


INSTALLATION/SETUP
------------

* Download/Clone the project.
* Inside Unity, import the project and run it.


LICENSES/TOOLS
------------

* Unity 2017/2018 - Game Engine
  Visit: https://unity3d.com/

* Adobe Photoshop - Sprite & Animation Creation
  Visit: https://www.adobe.com/de/products/photoshop.html

* GitKraken - Version Control Client
  Visit: https://www.gitkraken.com/

* BitBucket - Repositories
  Visit: https://bitbucket.org/

* Itch - Upload of Project Page
  Visit: https://itch.io/

* Visual Studio 2017 - Coding
  Visit: https://visualstudio.microsoft.com/

* Trello - Project Management (Kanban Style)
  Visit: https://trello.com/


TROUBLESHOOTING
---------------

 * If the project does not run properly, you found a bug or want to suggest a feature, write to one of the following:

   - Nikolas Klein:         nikolas.klein@ue-germany.de

FAQ
---

//no questions here yet

Q:

A:


 MAINTAINERS
 -----------

 Current maintainers:
  * Nikolas Klein (Programmer & Artist & Designer & everything else):             nikolas.klein@ue-germany.de